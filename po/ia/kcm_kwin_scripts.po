# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2012, 2013, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-22 02:45+0000\n"
"PO-Revision-Date: 2022-11-28 16:18+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: module.cpp:50
#, kde-format
msgid "Import KWin Script"
msgstr "Importa script de KWin"

#: module.cpp:51
#, kde-format
msgid "*.kwinscript|KWin scripts (*.kwinscript)"
msgstr "*.kwinscript|scripts de KWin (*.kwinscripts)"

#: module.cpp:62
#, kde-format
msgctxt "Placeholder is error message returned from the install service"
msgid ""
"Cannot import selected script.\n"
"%1"
msgstr ""
"Non pote importar le script seligite.\n"
"%1"

#: module.cpp:66
#, kde-format
msgctxt "Placeholder is name of the script that was imported"
msgid "The script \"%1\" was successfully imported."
msgstr "Le script \"%1\" esseva importate con successo."

#: module.cpp:125
#, kde-format
msgid "Error when uninstalling KWin Script: %1"
msgstr "Error quando deinstallava KWin Script: %1"

#: package/contents/ui/main.qml:47
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete..."
msgstr "Dele..."

#: package/contents/ui/main.qml:60
#, kde-format
msgid "Install from File..."
msgstr "Installa ex file ..."

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Get New Scripts..."
msgstr "Obtene nove scripts..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Giovanni Sora"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "g.sora@tiscali.it"

#~ msgid "KWin Scripts"
#~ msgstr "Scripts de KWin"

#~ msgid "Configure KWin scripts"
#~ msgstr "Configura Scripts de KWin"

#~ msgid "Tamás Krutki"
#~ msgstr "Tamás Krutki"

#~ msgid "KWin script configuration"
#~ msgstr "Configuration de script de KWin"

#~ msgid "Import KWin script..."
#~ msgstr "Importa script de KWin..."

#~ msgid ""
#~ "Cannot import selected script: maybe a script already exists with the "
#~ "same name or there is a permission problem."
#~ msgstr ""
#~ "Non pote importar script selectionate: forsan un script ja existe con le "
#~ "mesme nomine o on ha un problema de permissiones."
